# <strong>Projeto Back-end TT 22.1 - EJCM

## || Conteúdo do projeto
<br>

- JavaScript
- Modelagem ER
- CRUD/MVC
- Seeders

<hr>

<hr>

## Para rodar o projeto

#### 1) Clone esse repositório

git clone https://gitlab.com/joaoxvitor18/trabalhobackend-tt-22.1.git


#### 2) Entre na pasta do projeto

cd trabalhobackend-tt-22.1
cd trabalhoBackend


#### 3) Instale as dependências via comandos no terminal:

- npm install dotenv
- npm install faker-br
- npm install sequelize
- npm install node-dev
- npm install sqlite3
- npm install express

#### 4) Execute os comandos a seguir para começar a servir à API:

- npm run migrate
- npm run seed (Opcional para gerar um banco de dados aleatório)
- npm run dev

#### 5) Para parar de servir a API, basta executar o comando `CTRL + C` no terminal

#### 6) Instruções

- A modelagem ER se encontra em modelageER.brM3
- Para gerar novos usuários, deve-se usar o postman nos métodos e nas rotas explicitados em Routes.js
- Para visualizar todos os usuários ou produtos, pode-se usar o postman no método GET ou abrir o arquivo database.sqlite em DbBrowser
- Tavez seja necessário alterar o nome do arquivo ".env.example" para ".env"

<hr>

<div align="center">
Copyright © 2022<br> 
  ✨ Created by <b>João Assumpção</b> ✨
</div>

