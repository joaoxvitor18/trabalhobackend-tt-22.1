
const User = require('../models/User');
const Product = require('../models/Product')
const { response } = require('express');


const create = async(req, res) => {
    try{

        const product = await Product.create(req.body);
        return res.status(201).json({message: `${product.name} foi cadastrado com sucesso!`});

    } catch (err) {
        return res.status(500).json({error:err});
    }
};

const index = async(req,res) => {
    try {
        const products = await Product.findAll();
        return res.status(200).json({products});
    }catch(err){
        return res.status(500).json({err});
    }
};


const show = async(req,res) => {

    const {id} = req.params;
    
    try {
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    }catch(err){
        return res.status(500).json({err});
    }

};


const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updateVar] = await Product.update(req.body, {where: {id: id}});
        if(updateVar) {
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("O produto não existe no catálogo!");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json(`${product.name} removido(a) com sucesso`);
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("O produto não existe no catálogo!");
    }
};

const list = async(req,res) => {
    const {product_id, user_id} = req.params;
    try {
        const product = await Product.findByPk(product_id);
        const user = await User.findByPk(user_id);

        await product.setUser(user);
        return res.status(200).json({msg: `${user.name} listou ${product.name} com sucesso!`});
        
    } catch (error) {
        return res.status(500).json({error});
    }
}

const unlist = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        const user = await User.findByPk(product.dataValues.UserId);
        await product.setUser(null);
        return res.status(200).json({msg:`${user.name} removeu ${product.name} da lista de venda!`});
    } catch (error) {
        return res.status(500).json({error});
        
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    list,
    unlist
};