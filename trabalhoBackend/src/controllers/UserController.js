const { response } = require('express');
const User = require('../models/User');
const Product = require('../models/Product')

const create = async (req, res) => {
    try {
        const user = await User.create(req.body);
        return res.status(201).json({ message: `${user.name} foi cadastrado(a) com sucesso! `});
    } catch (err) {
        return res.status(500).json({ error: err });
    }
};

const index = async (req, res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({ users });

    } catch (err) {
        return res.status(500).json({ err });
    }
};


const show = async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({ user });

    } catch (err) {
        return res.status(500).json({ err });
    }
};
const update = async (req, res) => {
    const { id } = req.params;
    try {
        const [updateVar] = await User.update(req.body, { where: { id: id } });
        if (updateVar) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("O usuário não foi encontrado ou não está cadastrado!");
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await User.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json(`${user.name} foi descadastrado(a) com sucesso!`);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("O usuário não foi encontrado ou não está cadastrado!");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
};
