require('../../config/dotenv')();
require('../../config/sequelize');

const seedUser = require('./UserSeeder');
const seedProduct = require('./ProductSeeder');

(async () => {
  try {
    await seedUser();
    await seedProduct();

  } catch(err) { console.log(err) }
})();
