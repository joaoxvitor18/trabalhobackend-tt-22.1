const Product = require('../../models/Product');
const User = require('../../models/User');
const faker = require('faker-br');

const seedProduct = async function() {
    
    try {
        await Product.sync({ force: true });
        const products = [];
        const users = await User.findAll();
    
        for (let i = 0; i < 10; i++) {
          let product = await Product.create({
            price: faker.random.number(),
            name: faker.commerce.product(),
            description: faker.lorem.text(),
            status: faker.lorem.word(),
            cattegory: faker.lorem.word(),
            sold_by: faker.name.firstName(),
            quantity:faker.random.number()
          });

          let user = await User.findByPk(i+1);
          product.setUser(user);
          
          
        }
    
     } catch (err) { console.log(err); }
    }

module.exports = seedProduct;



