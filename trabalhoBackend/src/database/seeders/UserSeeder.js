const Product = require('../../models/Product');
const User = require('../../models/User');
const faker = require('faker-br');

const seedUser = async function() {

    const users = [];
  
    for (let i = 0; i < 10; i++) {
      users.push({
        name: faker.name.firstName(),
        username: faker.internet.userName(),
        password: faker.internet.password(),
        date_of_birth: faker.date.past(),
        rating: faker.random.float(),
        adress: faker.name.firstName()
      });
    }
  
    try {
      await User.sync({ force: true });
      await User.bulkCreate(users);
  
    } catch (err) { console.log(err); }
  }

module.exports = seedUser;

