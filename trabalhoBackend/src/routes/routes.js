const { Router } = require('express');
const UserController = require('../controllers/UserController');
const ProductController = require('../controllers/ProductController');
const router = Router();


router.get('/user',UserController.index);
router.get('/user/:id',UserController.show);
router.post('/user',UserController.create);
router.put('/user/:id',UserController.update);
router.delete('/user/:id',UserController.destroy);


router.get('/product',ProductController.index);
router.get('/product/:id',ProductController.show);
router.post('/product',ProductController.create);
router.put('/product/:id',ProductController.update);
router.delete('/product/:id',ProductController.destroy);
router.put('/product/list/:product_id/:user_id', ProductController.list);
router.put('/product/unlist/:id', ProductController.unlist);

module.exports = router;
